<?php

namespace App\DataFixtures;

use App\Entity\Categorie;
use App\Entity\Jeux;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $categorie1 = new Categorie();
        $categorie1->setLabel("fps");
        $manager->persist($categorie1);

        $categorie2 = new Categorie();
        $categorie2->setLabel("batlte royal");
        $manager->persist($categorie2);

        $categorie3 = new Categorie();
        $categorie3->setLabel("simulation");
        $manager->persist($categorie3);

        $Categorie = array();
        array_push($Categorie, $categorie1);
        array_push($Categorie, $categorie2);
        array_push($Categorie, $categorie3);

        
        for ($i = 0; $i < 20; $i++) {
            $cat = array_rand($Categorie,1);
            $jeux_video = new Jeux();
            $jeux_video->setNom("jeux_$i");
            $jeux_video->setDescription("Description du jeux_$i");
            $jeux_video->addCategory($Categorie[$cat]);
            $jeux_video->setUniqID(uniqid());
            $manager->persist($jeux_video);

        }


        $manager->flush();
    }
}
