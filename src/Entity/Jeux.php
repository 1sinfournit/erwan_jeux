<?php

namespace App\Entity;

use App\Entity\Categorie;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\JeuxRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: JeuxRepository::class)]
class Jeux
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[Groups(['jeux:read'])]
    #[ORM\Column(type: 'string', length: 255)]
    private $nom;

    #[Groups(['jeux:read'])]
    #[ORM\Column(type: 'string', length: 255)]
    private $description;

    #[Groups(['jeux:read'])]
    #[ORM\ManyToMany(targetEntity: Categorie::class, inversedBy: 'jeux')]
    private $Category;

    #[ORM\Column(type: 'string', length: 255)]
    private $uniqID;

    public function __construct()
    {
        $this->Category = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Collection<int, Categorie>
     */
    public function getCategory(): Collection
    {
        return $this->Category;
    }

    public function addCategory(Categorie $category): self
    {
        if (!$this->Category->contains($category)) {
            $this->Category[] = $category;
        }

        return $this;
    }

    public function removeCategory(Categorie $category): self
    {
        $this->Category->removeElement($category);

        return $this;
    }

    public function getUniqID(): ?string
    {
        return $this->uniqID;
    }

    public function setUniqID(string $uniqID): self
    {
        $this->uniqID = $uniqID;

        return $this;
    }
}
