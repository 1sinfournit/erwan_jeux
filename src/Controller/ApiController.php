<?php

namespace App\Controller;

use App\Entity\Categorie;
use App\Entity\Jeux;
use App\Repository\CategorieRepository;
use App\Repository\JeuxRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;


#[Route('/api', name: 'app_api')]
class ApiController extends AbstractController
{
    #[Route('/jeux/{id}', name: 'jeux_unique',methods:'get')]
    public function getOneJeux(JeuxRepository $jeuxRepository, string $id): Response
    {
        return $this->json($jeuxRepository->findBy(['uniqID' => $id]), 200, [], ['groups' => 'jeux:read']);

    }

    #[Route('/all/jeux', name: 'jeux_all_by',methods:'get')]
    public function getAllJeux(JeuxRepository $jeuxRepository,CategorieRepository $categorieRepository,  Request $request): Response
    {
        $nb_jeux = $request->query->get('nb_jeux');
        if(empty($nb_jeux))$nb_jeux = 10;
        $page = $request->query->get('page');
        if(empty($page))$nb_jeux = 1;
        $categorie_id = $request->query->get('categorie');
        if(empty($categorie_id)){
            $jeux = $jeuxRepository->findAll();
        }else{
            $categorie = $categorieRepository->find($categorie_id);
            $jeux = $categorie->getJeux();
        }
        return $this->json($jeuxRepository->findByCategory($jeux, $nb_jeux,$page), 200, [], ['groups' => 'jeux:read']);

    }
}
